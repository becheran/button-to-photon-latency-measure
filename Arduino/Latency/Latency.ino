// Uncomment if you just want to read the photodiode values
// #define CALIBRATE 1

// Set Pin Numbers
const int BUTTON_PIN = 5;                    // the number of the pushbutton pin
const int LED_PIN =  13;                     // the number of the LED pin
const int PHOTODIODE_PIN = A1;               // analog pin of phtotodiode
const uint16_t BRIGHTNESS_THRESHOLD = 5000;  // Brighness threshold used to detect button pressed

bool measure = false;
unsigned long startMeasureTime = 0;

void setup() {
  Serial.begin(9600);
  // initialize the LED pin as an output:
  pinMode(LED_PIN, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(BUTTON_PIN, INPUT);
  analogReadResolution(13); // The Teensy 3.2 has a 13 bit resolution ADC
  analogReadAveraging(1); // Reduce Signal Noise but introduces latency
}

void loop() {
#ifdef CALIBRATE    
    uint16_t photodiodeReading = analogRead(PHOTODIODE_PIN); 
    Serial.print("Sensor reading: "); 
    Serial.println(photodiodeReading);
#else  
  if(measure){
    digitalWrite(LED_PIN, HIGH);
    uint16_t photodiodeReading = analogRead(PHOTODIODE_PIN); 
    if(photodiodeReading > BRIGHTNESS_THRESHOLD){
      double latencyMS = (micros() - startMeasureTime) / 1000.0;
      Serial.print("Delay: ");
      Serial.print(latencyMS);
      Serial.println("ms");
      measure = false;
      delay(500);
    }
  } else {
    digitalWrite(LED_PIN, LOW);  
    if (digitalRead(BUTTON_PIN) == HIGH) {
      startMeasureTime = micros();
      measure = true;
    }
  }
#endif
}
