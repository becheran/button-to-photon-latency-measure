# Button-to-Photon Latency Measurement Tool

Measure end-to-end latency between button hit and display output via an external microcontroller, a photodiode and a push button.

## Setup
The repository consists of two separate parts. The Arduino project folder contains the external measurement system code and the Unity folder a simple test scenario to measure the delay.

### Microcontroller
For the project a microcontroller which can be programmed using the [Arduino](https://www.arduino.cc/en/main/software) environment is needed (IDE **version 1.8.4** was used). As microcontroller we used a [Teensy 3.2](https://www.pjrc.com/store/teensy32.html). Other boards are also possible. Just make sure that two analog input ports exist and that the built-in crystal is of good quality in order to get precise results with the Arduino time library.

To measure the button press time a simple push button needs to be attached to one of the analog inputs of the microcontroller. 

The [TSL250R](https://www.mouser.com/ds/2/588/TSL250R-TSL251R-TSL252R_DS000429_1-00-1149939.pdf) photodiode is used to detect light changes from dark to bright on the screen. The photo sensor needs to be attached to another analog input pin of the microcontroller.

![layout](_img/layout.jpg)

### Unity
For this project [Unity 3D](https://unity3d.com/de) **version 2017.2.2f1** was used to create a simple end-to-end test environment. Depending your specific setup you can of course use another software. It is just important that one part of the screen is turned from black to white when a button is pressed. The project contains only one simple scene which demonstrates the technique. Pressing the space-key changes the quadratic area in the center of the screen from black to white. 

## Calibrate
The analog inputs of the photosensor need to be interpreted as a simple binary value (display bright/display dark). In order to set the right brightness threshold value within the latency.ino file, a short calibration process is required. Just uncomment the the first pre-processor define in the calibration.ino file and compile and run the Arduino code. You should now get a console output of the current brightness value readings. Attach the diode to the display and make note of the bright and dark readings. You should now set the global *BRIGHTNESS_THRESHOLD* variable within the latency.ino file to a value between the brightest and darkest reading.

## Measure
To actually measure the end-to-end delay, compile and upload the Arduino code with the commented *CALIBRATE* pre-processor define. Attach the photodiode to the area on the display which changes from bright to dark and add to push-button right next to the key you programmed to change the area brightness (in the Unity 3D project it is the space-key). Start pressing the key simultaneously with the push button and you will get delay measurement outputs to the console. Repeat this process for several times and take the average over all measurements as end-to-end system delay. 