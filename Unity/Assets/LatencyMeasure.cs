﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class LatencyMeasure : MonoBehaviour {

    private Image _image;

    // Use this for initialization
    void Start () {
        _image = gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space)){
            _image.color = new Color(255, 255, 255);
        } else
        {
            _image.color = new Color(0, 0, 0);
        }
	}
}
